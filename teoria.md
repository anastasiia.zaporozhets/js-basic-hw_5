1. Як можна сторити функцію та як ми можемо її викликати?

Для того, щоб створити функцію спочатку потрібно її оголосити.
Оголосити функцію можна різними способами такими як:
1.Function Declaration:
Це спосіб оголошення функції, де ключове слово function використовується для створення нової функції. Тобто пишемо
function потім назву функції і спискок параметрів в дужках, якщо вони є. Якщо немає параметрів залишити пустими. Між
фігурними дужками буде наш код, тіло функції. Цей тип оголошення можна використовувати в будь-якому місці коду, навіть
перед викликом функції.
Наприклад:
function greet (name) {
return ("Hello, " + name + "!");
}

2.Function Expression:
Тут функція визначається як частина виразу або оператора присвоєння. Це означає, що ми можете присвоїти функцію змінній,
а потім використовувати цю змінну для виклику функції. Створюєму змінну називаємо її і потім присвоюємо їй function і
далі як Function Declaration.Цей спосіб не піднімає функцію тому його потрібно визначати перед використанням.
let greet = function(name) {
console.log("Hello, " + name + "!");
};

3.Arrow function - це новий синтаксис введений в ECMAScript 2015 (ES6), який надає більш короткий і зручний спосіб
визначення функцій в JavaScript. Тут замість слова function використовуємо стрілку (=>) для визначення функції. Також,
якщо функція має лише один аргумент, можна не писати дужки навколо нього:

let greet = name => {
console.log("Hello, " + name + "!");
};

Функції в JavaScript можна викликати, просто додавши дужки після їх імені, якщо функція приймає аргументи. 
Ось кілька прикладів виклику функцій:

greet();
greet("Anna" );

2. Що таке оператор return в JavaScript? Як його використовувати в функціях?
   Оператор return в використовується для повернення значення з функції. Коли виконується оператор return, він вказує на
   те, що виконання функції має бути завершене, і вказане значення (або undefined, якщо значення не вказане)
   повертається до місця виклику функції.

3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?
   Параметри - це змінні, які визначаються в оголошенні функції та приймають значення при її виклику. Параметри
   оголошуються в круглих дужках під час створення функції та використовуються для передачі даних у функцію.

Наприклад, у функції додати:
function add (а, b) {
return a + b;
}

Аргументи - це фактичні значення, які передаються у функцію під час її виклику. Аргументи передаються у вигляді значень
в дужках під час виклику функції.

Наприклад, у виклику функції add (3, 5), 3 та 5 - це аргументи, які передаються у функцію додати.
додати(3, 5); // Аргументи: 3 та 5

4. Як передати функцію аргументом в іншу функцію?
   Для того, щоб передати функцію аргументом в іншу функцію, потрібно використати функцію зворотного зв'язку (callback
   function)
   Це функція, яка передається як аргумент у іншу функцію та викликається пізніше, коли виконується певна умова або
   завдання. Функції зворотнього виклику дозволяють програмі працювати в асинхронному режимі, очікуючи на результати або
   події, не блокуючи виконання іншого коду.
   Основні характеристики функцій зворотнього виклику:

Передаються як аргументи до іншої функції.
Викликаються пізніше, коли виконується певна умова або завдання.
Використовуються для асинхронних операцій та обробки подій.

Ось приклад короткої функції зворотнього виклику:

function process(a) {
a("перша функція");
}

function callbackFunction(b) {
console.log("Результат з функції зворотнього виклику:", b);
}
process(callbackFunction);









